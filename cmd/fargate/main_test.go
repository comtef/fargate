package main

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/fargate/config"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/fargate/internal/cli"
)

func TestLoadCliArgsEnvVars(t *testing.T) {
	originalTaskDefinition := "default-task-def:1"
	originalPlatformVersion := "LATEST"
	originalRoleToAssumeARN := "arn:aws:iam::xxxxxxxxx:role/role-name"
	originalVariablesToAddToSession := "CI_PROJECT_ID"
	overrideTaskDefinition := "another-task-def:1"
	overridePlatformVersion := "1.4.0"
	overrideRoleToAssumeARN := "arn:aws:iam::xxxxxxxxx:role/another-role-name"
	overrideVariablesToAddToSession := "CI_PROJECT_ID,CI_PROJECT_PATH"

	tests := map[string]struct {
		taskDefinitionOverrideValue          string
		platformVersionOverrideValue         string
		roleToAssumeARNOverrideValue         string
		variablesToAddToSessionOverrideValue string
		expectedTaskDefinition               string
		expectedPlatformVersion              string
		expectedTaskRoleToAssumeARN          string
		expectedVariablesToAddToSession      string
	}{
		"Should keep original values if nothing received by command line or env variable": {
			taskDefinitionOverrideValue:     "",
			expectedTaskDefinition:          originalTaskDefinition,
			expectedPlatformVersion:         originalPlatformVersion,
			expectedTaskRoleToAssumeARN:     originalRoleToAssumeARN,
			expectedVariablesToAddToSession: originalVariablesToAddToSession,
		},
		"Should override task definition if received by command line or env variable": {
			taskDefinitionOverrideValue:     overrideTaskDefinition,
			expectedTaskDefinition:          overrideTaskDefinition,
			expectedPlatformVersion:         originalPlatformVersion,
			expectedTaskRoleToAssumeARN:     originalRoleToAssumeARN,
			expectedVariablesToAddToSession: originalVariablesToAddToSession,
		},
		"Should override platform version if received by command line or env variable": {
			platformVersionOverrideValue:    overridePlatformVersion,
			expectedTaskDefinition:          originalTaskDefinition,
			expectedPlatformVersion:         overridePlatformVersion,
			expectedTaskRoleToAssumeARN:     originalRoleToAssumeARN,
			expectedVariablesToAddToSession: originalVariablesToAddToSession,
		},
		"Should override platform version and task definition if received by command line or env variable": {
			taskDefinitionOverrideValue:     overrideTaskDefinition,
			platformVersionOverrideValue:    overridePlatformVersion,
			expectedTaskDefinition:          overrideTaskDefinition,
			expectedPlatformVersion:         overridePlatformVersion,
			expectedTaskRoleToAssumeARN:     originalRoleToAssumeARN,
			expectedVariablesToAddToSession: originalVariablesToAddToSession,
		},
		"Should override role arn if received by command line or env variable": {
			roleToAssumeARNOverrideValue:    overrideRoleToAssumeARN,
			expectedTaskDefinition:          originalTaskDefinition,
			expectedPlatformVersion:         originalPlatformVersion,
			expectedTaskRoleToAssumeARN:     overrideRoleToAssumeARN,
			expectedVariablesToAddToSession: originalVariablesToAddToSession,
		},
		"Should override role arn and variables if received by command line or env variable": {
			roleToAssumeARNOverrideValue:         overrideRoleToAssumeARN,
			variablesToAddToSessionOverrideValue: overrideVariablesToAddToSession,
			expectedTaskDefinition:               originalTaskDefinition,
			expectedPlatformVersion:              originalPlatformVersion,
			expectedTaskRoleToAssumeARN:          overrideRoleToAssumeARN,
			expectedVariablesToAddToSession:      overrideVariablesToAddToSession,
		},
	}

	for tn, tt := range tests {
		t.Run(tn, func(t *testing.T) {
			oldTaskDefGlobalValue := global.TaskDefinition
			oldPlatformVersionGlobalValue := global.PlatformVersion
			oldRoleARNGlobalValue := global.RoleToAssumeARN
			oldVariablesGlobalValue := global.VariablesAddedToSTSSession
			global.TaskDefinition = tt.taskDefinitionOverrideValue
			global.PlatformVersion = tt.platformVersionOverrideValue
			global.RoleToAssumeARN = tt.roleToAssumeARNOverrideValue
			global.VariablesAddedToSTSSession = tt.variablesToAddToSessionOverrideValue

			defer func() {
				global.TaskDefinition = oldTaskDefGlobalValue
				global.PlatformVersion = oldPlatformVersionGlobalValue
				global.RoleToAssumeARN = oldRoleARNGlobalValue
				global.VariablesAddedToSTSSession = oldVariablesGlobalValue
			}()

			testContext := createCliContextForTests(originalTaskDefinition, originalPlatformVersion, originalRoleToAssumeARN, originalVariablesToAddToSession)
			assert.Equal(t, originalTaskDefinition, testContext.Config().Fargate.TaskDefinition)
			assert.Equal(t, originalPlatformVersion, testContext.Config().Fargate.PlatformVersion)
			assert.Equal(t, originalRoleToAssumeARN, testContext.Config().STSConfiguration.RoleToAssumeARN)
			assert.Equal(t, originalVariablesToAddToSession, testContext.Config().STSConfiguration.VariablesAddedToSTSSession)

			err := loadCliArgsEnvVars(testContext)
			assert.NoError(t, err)
			assert.Equal(t, tt.expectedTaskDefinition, testContext.Config().Fargate.TaskDefinition)
			assert.Equal(t, tt.expectedPlatformVersion, testContext.Config().Fargate.PlatformVersion)
			assert.Equal(t, tt.expectedTaskRoleToAssumeARN, testContext.Config().STSConfiguration.RoleToAssumeARN)
			assert.Equal(t, tt.expectedVariablesToAddToSession, testContext.Config().STSConfiguration.VariablesAddedToSTSSession)
		})
	}
}

func createCliContextForTests(taskDefinition string, platformVersion string, roleToAssumeARN string, variablesAddedToSTSSession string) *cli.Context {
	cliCtx := new(cli.Context)
	cliCtx.SetConfig(config.Global{
		Fargate: config.Fargate{
			TaskDefinition:  taskDefinition,
			PlatformVersion: platformVersion,
		},
		STSConfiguration: config.STSConfiguration{
			RoleToAssumeARN:            roleToAssumeARN,
			VariablesAddedToSTSSession: variablesAddedToSTSSession,
		},
	})

	return cliCtx
}
